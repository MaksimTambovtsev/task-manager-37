package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.*;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@Nullable final ITaskRepository repository,
                       @NotNull IConnectionService connection) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connectionDB);
        try {
            @NotNull final Task task = new Task();
            task.setUserId(userId);
            task.setName(name);
            task.setDescription(description);
            repository.create(task);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId, @Nullable final String id,
                           @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connectionDB);
        try {
            @Nullable final Task task = repository.findById(userId, id);
            if (task == null) return null;
            task.setName(name);
            task.setDescription(description);
            task.setUserId(userId);
            repository.updateById(task);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id,
                                     @Nullable final Status status) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connectionDB);
        try {
            final Task task = repository.findById(userId, id);
            if (task == null) return null;
            task.setStatus(status);
            task.setUserId(userId);
            repository.updateById(task);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

}
