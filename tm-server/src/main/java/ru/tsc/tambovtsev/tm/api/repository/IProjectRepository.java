package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    void create(@NotNull Project project);

    void updateById(@NotNull Project project);

}