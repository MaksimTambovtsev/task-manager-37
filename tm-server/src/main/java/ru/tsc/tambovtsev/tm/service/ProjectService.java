package ru.tsc.tambovtsev.tm.service;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository,
                          @NotNull IConnectionService connection) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connectionDB);
        try {
            @NotNull final Project project = new Project();
            project.setUserId(userId);
            project.setName(name);
            project.setDescription(description);
            repository.create(project);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String userId, @Nullable final String id,
                              @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connectionDB);
        try {
            final Project project = repository.findById(userId, id);
            if (project == null) return null;
            project.setName(name);
            project.setDescription(description);
            project.setUserId(userId);
            repository.updateById(project);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id,
                                           @Nullable final Status status) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connectionDB);
        try {
            final Project project = repository.findById(userId, id);
            if (project == null) return null;
            project.setStatus(status);
            project.setUserId(userId);
            repository.updateById(project);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

}
