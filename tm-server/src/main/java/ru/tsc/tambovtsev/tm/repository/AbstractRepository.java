package ru.tsc.tambovtsev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractEntity> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    @NotNull
    protected final List<M> models = new ArrayList<>();

    protected AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    protected abstract String getTableName();

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @NotNull final M result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) {
        @NotNull final String sql = "DELETE FROM " + getTableName() +
                " WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        return null;
    }

}
