package ru.tsc.tambovtsev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_TASK";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task create(@NotNull Task task) {
        @NotNull final String sql = "INSERT " + getTableName() +
                "(ID, NAME, DESCRIPTION, STATUS, USER_ID) VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getStatus().name());
        statement.setString(5, task.getUserId());
        statement.executeUpdate();
        statement.close();
        return task;
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull Task task) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET NAME = ?, DESCRIPTION = ?, STATUS = ?, USER_ID = ?, PROJECT_ID = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setString(3, task.getStatus().name());
        statement.setString(4, task.getUserId());
        statement.setString(5, task.getProjectId());
        statement.setString(6, task.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        @NotNull final String sql = "DELETE FROM " + getTableName() +
                " WHERE PROJECT_ID = ? AND USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        task.setUserId(row.getString("USER_ID"));
        task.setStatus(Status.valueOf(row.getString("STATUS")));
        task.setProjectId(row.getString("PROJECT_ID"));
        return task;
    }

}
