package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IOwnerRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IUserOwnedService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@Nullable final R repository, @NotNull IConnectionService connection) {
        super(repository, connection);
    }

    @NotNull
    public abstract IOwnerRepository<M> getRepository(@NotNull Connection connection);

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IOwnerRepository<M> repository = getRepository(connectionDB);
        try {
            repository.clear(userId);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IOwnerRepository<M> repository = getRepository(connectionDB);
        try {
            @Nullable final M result = repository.findById(userId, id);
            return result;
        }
        finally {
            connectionDB.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IOwnerRepository<M> repository = getRepository(connectionDB);
        try {
            @Nullable final int result = repository.getSize(userId);
            return result;
        }
        finally {
            connectionDB.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IOwnerRepository<M> repository = getRepository(connectionDB);
        try {
            @Nullable final M result = repository.findById(userId, id);
            if (result == null) return null;
            repository.removeById(userId, id);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IOwnerRepository<M> repository = getRepository(connectionDB);
        try {
            @Nullable final List<M> result = repository.findAll(userId, sort);
            return result;
        }
        finally {
            connectionDB.close();
        }
    }

}
