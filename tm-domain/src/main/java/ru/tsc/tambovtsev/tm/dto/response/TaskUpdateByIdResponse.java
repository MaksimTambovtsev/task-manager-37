package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

@NoArgsConstructor
public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@Nullable Task task) {
        super(task);
    }

}
